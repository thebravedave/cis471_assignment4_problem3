import java.util.*;

public class Main {

    private static Boolean[] outcomesOnDays = new Boolean[13];
    public static Integer nodesOrder = 0;

    public static List<StringBuilder> statisticalInformation = new ArrayList<StringBuilder>();

    public static Attribute rootNode = null;

    public static void main(String[] args) {



        outcomesOnDays[0] = null;
        outcomesOnDays[1] = true;
        outcomesOnDays[2] = false;
        outcomesOnDays[3] = true;
        outcomesOnDays[4] = true;
        outcomesOnDays[5] = false;

        outcomesOnDays[6] = true;
        outcomesOnDays[7] = false;
        outcomesOnDays[8] = true;
        outcomesOnDays[9] = false;

        outcomesOnDays[10] = false;
        outcomesOnDays[11] = false;
        outcomesOnDays[12] = true;

        System.out.println("Hello World!");

        /*==========================================================*/
        /*==========================================================*/
        Attribute alternative = new Attribute();
        alternative.attributeName = "Alternative";


        Subtable alternativeYesSubTable = new Subtable();
        alternativeYesSubTable.subtableName = "yes";
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(1);
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(2);
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(4);
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(5);
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(10);
        alternativeYesSubTable.daysValueWasFoundOnForAttribute.add(12);
        alternative.values.add(alternativeYesSubTable);


        Subtable alternativeNoSubTable = new Subtable();
        alternativeNoSubTable.subtableName = "no";
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(3);
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(6);
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(7);
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(8);
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(9);
        alternativeNoSubTable.daysValueWasFoundOnForAttribute.add(11);
        alternative.values.add(alternativeNoSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute bar = new Attribute();
        bar.attributeName = "Bar";


        Subtable barYesSubTable = new Subtable();
        barYesSubTable.subtableName = "yes";
        barYesSubTable.daysValueWasFoundOnForAttribute.add(3);
        barYesSubTable.daysValueWasFoundOnForAttribute.add(6);
        barYesSubTable.daysValueWasFoundOnForAttribute.add(7);
        barYesSubTable.daysValueWasFoundOnForAttribute.add(9);
        barYesSubTable.daysValueWasFoundOnForAttribute.add(10);
        barYesSubTable.daysValueWasFoundOnForAttribute.add(12);
        bar.values.add(barYesSubTable);


        Subtable barNoSubTable = new Subtable();
        barNoSubTable.subtableName = "no";
        barNoSubTable.daysValueWasFoundOnForAttribute.add(1);
        barNoSubTable.daysValueWasFoundOnForAttribute.add(2);
        barNoSubTable.daysValueWasFoundOnForAttribute.add(4);
        barNoSubTable.daysValueWasFoundOnForAttribute.add(5);
        barNoSubTable.daysValueWasFoundOnForAttribute.add(8);
        barNoSubTable.daysValueWasFoundOnForAttribute.add(11);
        bar.values.add(barNoSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute friSat = new Attribute();
        friSat.attributeName = "FridayOrSaturday";

        Subtable friSatYesSubTable = new Subtable();
        friSatYesSubTable.subtableName = "yes";
        friSatYesSubTable.daysValueWasFoundOnForAttribute.add(4);
        friSatYesSubTable.daysValueWasFoundOnForAttribute.add(5);
        friSatYesSubTable.daysValueWasFoundOnForAttribute.add(9);
        friSatYesSubTable.daysValueWasFoundOnForAttribute.add(10);
        friSatYesSubTable.daysValueWasFoundOnForAttribute.add(12);

        friSat.values.add(friSatYesSubTable);

        Subtable friSatNoSubTable = new Subtable();
        friSatNoSubTable.subtableName = "no";
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(1);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(2);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(3);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(6);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(7);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(8);
        friSatNoSubTable.daysValueWasFoundOnForAttribute.add(11);

        friSat.values.add(friSatNoSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute patrons = new Attribute();
        patrons.attributeName = "Patrons";

        Subtable patronsSomeSubTable = new Subtable();
        patronsSomeSubTable.subtableName = "some";
        patronsSomeSubTable.daysValueWasFoundOnForAttribute.add(1);
        patronsSomeSubTable.daysValueWasFoundOnForAttribute.add(3);
        patronsSomeSubTable.daysValueWasFoundOnForAttribute.add(6);
        patronsSomeSubTable.daysValueWasFoundOnForAttribute.add(8);
        patrons.values.add(patronsSomeSubTable);


        Subtable patronsNoneSubTable = new Subtable();
        patronsNoneSubTable.subtableName = "none";
        patronsNoneSubTable.daysValueWasFoundOnForAttribute.add(7);
        patronsNoneSubTable.daysValueWasFoundOnForAttribute.add(11);
        patrons.values.add(patronsNoneSubTable);


        Subtable patronsFullSubTable = new Subtable();
        patronsFullSubTable.subtableName = "full";
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(2);
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(4);
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(5);
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(9);
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(10);
        patronsFullSubTable.daysValueWasFoundOnForAttribute.add(12);
        patrons.values.add(patronsFullSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute price = new Attribute();
        price.attributeName = "Price";


        Subtable priceLowSubTable = new Subtable();
        priceLowSubTable.subtableName = "low";
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(2);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(3);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(4);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(7);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(9);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(11);
        priceLowSubTable.daysValueWasFoundOnForAttribute.add(12);
        price.values.add(priceLowSubTable);


        Subtable priceMediumSubTable = new Subtable();
        priceMediumSubTable.subtableName = "medium";
        priceMediumSubTable.daysValueWasFoundOnForAttribute.add(6);
        priceMediumSubTable.daysValueWasFoundOnForAttribute.add(8);
        price.values.add(priceMediumSubTable);


        Subtable priceHighSubTable = new Subtable();
        priceHighSubTable.subtableName = "high";
        priceHighSubTable.daysValueWasFoundOnForAttribute.add(1);
        priceHighSubTable.daysValueWasFoundOnForAttribute.add(5);
        priceHighSubTable.daysValueWasFoundOnForAttribute.add(10);
        price.values.add(priceHighSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute raining = new Attribute();
        raining.attributeName = "Raining";


        Subtable rainingYesSubTable = new Subtable();
        rainingYesSubTable.subtableName = "yes";
        rainingYesSubTable.daysValueWasFoundOnForAttribute.add(6);
        rainingYesSubTable.daysValueWasFoundOnForAttribute.add(7);
        rainingYesSubTable.daysValueWasFoundOnForAttribute.add(8);
        rainingYesSubTable.daysValueWasFoundOnForAttribute.add(9);
        raining.values.add(rainingYesSubTable);


        Subtable rainingNoSubTable = new Subtable();
        rainingNoSubTable.subtableName = "no";
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(1);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(2);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(3);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(4);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(5);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(10);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(11);
        rainingNoSubTable.daysValueWasFoundOnForAttribute.add(12);
        raining.values.add(rainingNoSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute reservation = new Attribute();
        reservation.attributeName = "Reservation";


        Subtable reservationYesSubTable = new Subtable();
        reservationYesSubTable.subtableName = "yes";
        reservationYesSubTable.daysValueWasFoundOnForAttribute.add(1);
        reservationYesSubTable.daysValueWasFoundOnForAttribute.add(5);
        reservationYesSubTable.daysValueWasFoundOnForAttribute.add(6);
        reservationYesSubTable.daysValueWasFoundOnForAttribute.add(8);
        reservationYesSubTable.daysValueWasFoundOnForAttribute.add(10);
        reservation.values.add(reservationYesSubTable);


        Subtable reservationNoSubTable = new Subtable();
        reservationNoSubTable.subtableName = "no";
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(2);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(3);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(4);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(7);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(9);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(11);
        reservationNoSubTable.daysValueWasFoundOnForAttribute.add(12);
        reservation.values.add(reservationNoSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute type = new Attribute();
        type.attributeName = "Type";


        Subtable typeFrenchSubTable = new Subtable();
        typeFrenchSubTable.subtableName = "french";
        typeFrenchSubTable.daysValueWasFoundOnForAttribute.add(1);
        typeFrenchSubTable.daysValueWasFoundOnForAttribute.add(5);
        type.values.add(typeFrenchSubTable);


        Subtable typeThaiSubTable = new Subtable();
        typeThaiSubTable.subtableName = "thai";
        typeThaiSubTable.daysValueWasFoundOnForAttribute.add(2);
        typeThaiSubTable.daysValueWasFoundOnForAttribute.add(4);
        typeThaiSubTable.daysValueWasFoundOnForAttribute.add(8);
        typeThaiSubTable.daysValueWasFoundOnForAttribute.add(11);
        type.values.add(typeThaiSubTable);


        Subtable typeBurgerSubTable = new Subtable();
        typeBurgerSubTable.subtableName = "burger";
        typeBurgerSubTable.daysValueWasFoundOnForAttribute.add(3);
        typeBurgerSubTable.daysValueWasFoundOnForAttribute.add(7);
        typeBurgerSubTable.daysValueWasFoundOnForAttribute.add(9);
        typeBurgerSubTable.daysValueWasFoundOnForAttribute.add(12);
        type.values.add(typeBurgerSubTable);


        Subtable typeItalianSubTable = new Subtable();
        typeItalianSubTable.subtableName = "italian";
        typeItalianSubTable.daysValueWasFoundOnForAttribute.add(6);
        typeItalianSubTable.daysValueWasFoundOnForAttribute.add(10);
        type.values.add(typeItalianSubTable);
        /*==========================================================*/
        /*==========================================================*/
        Attribute waitEstimate = new Attribute();
        waitEstimate.attributeName = "Wait";


        Subtable waitZeroToTenSubTable = new Subtable();
        waitZeroToTenSubTable.subtableName = "zero to ten";
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(1);
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(3);
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(6);
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(7);
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(8);
        waitZeroToTenSubTable.daysValueWasFoundOnForAttribute.add(11);
        waitEstimate.values.add(waitZeroToTenSubTable);


        Subtable waitTenToThirtySubTable = new Subtable();
        waitTenToThirtySubTable.subtableName = "ten to thirty";
        waitTenToThirtySubTable.daysValueWasFoundOnForAttribute.add(4);
        waitTenToThirtySubTable.daysValueWasFoundOnForAttribute.add(10);
        waitEstimate.values.add(waitTenToThirtySubTable);


        Subtable waitThirtyToSixtySubTable = new Subtable();
        waitThirtyToSixtySubTable.subtableName = "thirty to sixty";
        waitThirtyToSixtySubTable.daysValueWasFoundOnForAttribute.add(2);
        waitThirtyToSixtySubTable.daysValueWasFoundOnForAttribute.add(12);
        waitEstimate.values.add(waitThirtyToSixtySubTable);


        Subtable waitMoreThanSixtySubTable = new Subtable();
        waitMoreThanSixtySubTable.subtableName = "more than sixty";
        waitMoreThanSixtySubTable.daysValueWasFoundOnForAttribute.add(5);
        waitMoreThanSixtySubTable.daysValueWasFoundOnForAttribute.add(9);
        waitEstimate.values.add(waitMoreThanSixtySubTable);
        /*==========================================================*/
        /*==========================================================*/




        List<Attribute> attributesList = new ArrayList<>();
        attributesList.add(type);
        attributesList.add(patrons);
        attributesList.add(alternative);
        attributesList.add(bar);
        attributesList.add(friSat);
        attributesList.add(price);
        attributesList.add(raining);
        attributesList.add(reservation);
        attributesList.add(waitEstimate);



        //after we call this we will have all of our Attributes with each having a Information Gain.
        calculateInformationGainForList(attributesList, null);

        writeToStatisticalInformationArrayList(rootNode, attributesList, "None since this is the root");

        //We need to get our root node which will be the node with the most information gain.
        rootNode = getAttributeWithMostInformationGain(attributesList);

        rootNode.nodesOrder = nodesOrder;
        nodesOrder++;




       // try {
            assignChildAttributesToParent(rootNode, attributesList);
      //  } catch (Exception e) {
         //   System.out.println("Error:" + e.getMessage());
       // }



        //Now our rootNode should have the complete tree graph attached to it.


        System.out.println("test");

        for(StringBuilder currentStringBuilder : statisticalInformation){

            System.out.println(currentStringBuilder);

        }
    }

    private static void assignChildAttributesToParent(Attribute parentAttribute, List<Attribute> attributesList){

        //need to check to see if the attributeList is empty or if the parentAttribute has all pure values
        //If so then we need to return;
        //todo i think we can get rid of this since we are checking all of this attributes values for purity below and skipping pure tables
        if(areAllSubtablesForAttributePure(parentAttribute) || attributesList.size() == 0){
            return;
        }

        for(Subtable currentSubtable : parentAttribute.values){


            //todo need to check if this subtable is pure
            if(checkIfSubtableIsPure(currentSubtable.daysValueWasFoundOnForAttribute)){
                //then we need to find another subtable
                continue;
            }

            //need to clone the attributesList
            List<Attribute> clonedAttributeList = cloneAttributeList(attributesList);

            //     Double[] parentAttributePositivesAndNegatives = getPositivesAndNegatives(currentSubtable.daysValueWasFoundOnForAttribute);

            calculateInformationGainForList(clonedAttributeList, currentSubtable);


            //write to our statistical information for printing out at end
            writeToStatisticalInformationArrayList(parentAttribute, clonedAttributeList, currentSubtable.subtableName);

            Attribute childAttribute = getAttributeWithMostInformationGain(clonedAttributeList);
            childAttribute.nodesOrder = nodesOrder;
            nodesOrder++;

            //need to set parentNode's currentBestSubtable to have childAttribute as it's child attribute
            currentSubtable.childAttribute = childAttribute;



            //this will remove the days that do not correspond to the chosen parentAttributes value.
            removeDaysFromChildAttributeAndRemainingAttributesListThatAreNotPartOfParentValuesDays(currentSubtable, childAttribute, clonedAttributeList);

            //we need to call this method again recursively to fill out the rest of this branch
            assignChildAttributesToParent(childAttribute, clonedAttributeList);

            //todo need to write to parentAttribute's "report"

        }
    }
    private static void removeDaysFromChildAttributeAndRemainingAttributesListThatAreNotPartOfParentValuesDays(Subtable parentAttributesSubtable, Attribute childAttribute, List<Attribute> attributesList){

        for(Subtable childAttributeSubtable : childAttribute.values){
            removeParentsDaysFromOtherAttributesDay(parentAttributesSubtable, childAttributeSubtable);
        }
        for(Attribute currentAttribute : attributesList){

            for(Subtable currentAttributesSubtable : currentAttribute.values){
                removeParentsDaysFromOtherAttributesDay(parentAttributesSubtable, currentAttributesSubtable);
            }

        }

    }
    private static void removeParentsDaysFromOtherAttributesDay(Subtable parentSubtable, Subtable otherAttributeSubtable){

        ArrayList<Integer> valuesToBeRemoved = new ArrayList<Integer>();
        for(Integer otherAttributeDay : otherAttributeSubtable.daysValueWasFoundOnForAttribute){

            if(!parentSubtable.daysValueWasFoundOnForAttribute.contains(otherAttributeDay)){
                //then we want to remove it from the otherAttributeSubtablesDays
                valuesToBeRemoved.add(otherAttributeDay);
            }

        }
        for(Integer valueToRemove : valuesToBeRemoved){
            otherAttributeSubtable.daysValueWasFoundOnForAttribute.remove(valueToRemove);
        }
    }

    private static Double[] getPositivesAndNegatives(List<Integer> daysValueWasFoundOnForAttribute){
        Double numberPositives = 0.0;
        Double numberNegatives = 0.0;

        for(Integer currentDayIndex : daysValueWasFoundOnForAttribute){

            boolean dayWaitValue = outcomesOnDays[currentDayIndex];
            if(dayWaitValue){
                numberPositives++;
            }
            else{
                numberNegatives++;
            }
        }

        Double[] positivesAndNegatives = new Double[2];
        positivesAndNegatives[0] = numberPositives;
        positivesAndNegatives[1] = numberNegatives;

        return positivesAndNegatives;

    }
    private static Double[] getPositivesAndNegativesForChildAttributeBasedOnItsParentAttributesValuesSubtable(Subtable parentAttributesSubtable, Subtable childAttributesSubtable){

        Double numberNegatives = 0.0;
        Double numberPositives = 0.0;

        if(parentAttributesSubtable == null){
            //then this is for a potential root node and we want to get all of the positives and negatives for this attribute
            for(Integer currentDay : childAttributesSubtable.daysValueWasFoundOnForAttribute){

                if(outcomesOnDays[currentDay].equals(true)){
                    numberPositives++;
                }
                else{
                    numberNegatives++;
                }
            }
        }
        else{
            //else this is for a potential child node (not a root node) and we need to only count the days that the parent attribute's value's subtable
            for(Integer currentDay : parentAttributesSubtable.daysValueWasFoundOnForAttribute){

                //need to make sure childAttribute has "current day" in it's own daysValueWasFoundOnForAttribute.
                //if not then this "currentDay" is for a different value for the child attribute and we should not count it
                if(childAttributesSubtable.daysValueWasFoundOnForAttribute.contains(currentDay)){

                    //then this "currentDay" falls on the same day as our child attributes value, so we can count it as either a negative or a positive
                    if(outcomesOnDays[currentDay].equals(true)){
                        numberPositives++;
                    }
                    else{
                        numberNegatives++;
                    }
                }
            }
        }


        Double[] negativesAndPositivesForChildAttributesValue = new Double[2];
        negativesAndPositivesForChildAttributesValue[0] = numberPositives;
        negativesAndPositivesForChildAttributesValue[1] = numberNegatives;
        return negativesAndPositivesForChildAttributesValue;
    }



    private static  List<Attribute> cloneAttributeList(List<Attribute> attributeList){

        try {


            List<Attribute> clonedAttributeList = new ArrayList<>();

            for (Attribute currentAttribute : attributeList) {

                Attribute clonedAttribute = currentAttribute.deepClone();
                clonedAttributeList.add(clonedAttribute);
            }
            return clonedAttributeList;
        }
        catch(Exception e){
            return null;
        }

    }

    //todo need to adjust this using "outcomesOnDays"
    private static boolean areAllSubtablesForAttributePure(Attribute attribute){


        boolean allPure = true;

        for(Subtable currentSubtable : attribute.values){

            if(!checkIfSubtableIsPure(currentSubtable.daysValueWasFoundOnForAttribute)){
                allPure = false;
            }
        }
        return allPure;

    }
    private static boolean checkIfSubtableIsPure(List<Integer> daysValueWasFoundOnForAttribute){
        Double[] positiveNegative =getPositivesAndNegatives(daysValueWasFoundOnForAttribute);

        if((!positiveNegative[0].equals(0.0) && !positiveNegative[1].equals(0.0))){
            //then this subtable is not pure
            return false;

        }
        return true;
    }


    private static Attribute getAttributeWithMostInformationGain(List<Attribute> attributeList){

        int index = 0;
        int mostInformationGainAttributeIndex = 0;
        Attribute mostInformationGainAttribute = null;

        for(Attribute currentAttribute : attributeList){
            if(mostInformationGainAttribute == null){
                mostInformationGainAttribute = currentAttribute;
                index++;
                continue;
            }
            if(Double.compare(currentAttribute.informationGain, mostInformationGainAttribute.informationGain) > 0){

                //then the currentAttribute's information gain is greater than the mostInformationGainAttribute so we
                //need to make mostInformationGainAttribute equal to currentAttribute
                mostInformationGainAttribute = currentAttribute;
                mostInformationGainAttributeIndex = index;
            }

            index++;

        }
        //now that we know the attribute with the most information gain and it's index.  We need to remove it from the attributeList and return it.
        attributeList.remove(mostInformationGainAttributeIndex);
        return mostInformationGainAttribute;

    }


    public static void calculateInformationGainForList(List<Attribute> attributes, Subtable parentAttributesSubtable){

        Double[] parentAttibutesSubtablesPositiveNegative = null;
        if(parentAttributesSubtable != null){
            parentAttibutesSubtablesPositiveNegative = getPositivesAndNegatives(parentAttributesSubtable.daysValueWasFoundOnForAttribute);

        }



        for(Attribute currentAttribute : attributes){
            Double currentAttributeTotalRemainder = 0.0;



            /*  p = positive examples   n = negative examples

            I(  (p/p+n), (n/p+n)    )      =            -(p/p+n) *  log2(  p/p+n )  -  (n/p+n) * log2( n/p+n )

            Remainder for a value of attribute:

            ( (pi + ni) / (p+n) )   *     (  -(p/p+n) *  log2(  p/p+n )  -  (n/p+n) * log2( n/p+n )   )

            =

            ( (pi + ni) / 12 )      *    (   -(pi/pi+ni) *  log2(  pi/pi+ni )  -  (ni/pi+ni) * log2( ni/pi+ni )     )
                   part1                           part2          part3              part4           part5
            */

            /*======================================================
            ========================================================
            F I R S T   C A L C U L A T E   T H E    R E M A I N D E R
            ========================================================
             =======================================================*/
            for(Subtable currentSubtable : currentAttribute.values){


                String attributeName = currentAttribute.attributeName;
                String valueName = currentSubtable.subtableName;

                Double[] positiveNegativeArray = getPositivesAndNegativesForChildAttributeBasedOnItsParentAttributesValuesSubtable(parentAttributesSubtable, currentSubtable);


               // if(positiveNegativeArray[0].equals(0.0) || positiveNegativeArray[1].equals(0.0)){

                    //then this is a "pure" attribute value and we don't need to perform any calculations
                    //todo makes sure this reasoning is sound
                 //   continue;
                //}


                Double totalNumberOfPositivesAndNegatives = 12.0;
                if(parentAttibutesSubtablesPositiveNegative != null){
                    totalNumberOfPositivesAndNegatives = parentAttibutesSubtablesPositiveNegative[0] + parentAttibutesSubtablesPositiveNegative[1];
                }

                Double part1 = 0.0;
                if(!totalNumberOfPositivesAndNegatives.equals(0.0)){
                    part1 = ( (positiveNegativeArray[0] + positiveNegativeArray[1])/totalNumberOfPositivesAndNegatives );
                }


                Double informationContent = calculateInformationContent(positiveNegativeArray);

                Double remainderForValueI = part1 * informationContent;

                currentSubtable.informationContent = remainderForValueI;

                currentAttributeTotalRemainder += remainderForValueI;

            }

            Double parentAttributeSubTableInformationContent = calculateInformationContent(parentAttibutesSubtablesPositiveNegative);

            Double childAttributeInformationGain = parentAttributeSubTableInformationContent - currentAttributeTotalRemainder;
            currentAttribute.informationGain = childAttributeInformationGain;


        }
    }


    private static Double calculateInformationContent(Double[] positiveNegativeArray){


        /*
                 (   -(pi/pi+ni) *  log2(  pi/pi+ni )  -  (ni/pi+ni) * log2( ni/pi+ni )     )
                         part2          part3              part4           part5
         */

        if(positiveNegativeArray == null){
            return 1.0;
        }
        if(positiveNegativeArray[0].equals(0.0) && positiveNegativeArray[1].equals(0.0)){
            return 0.0;
        }

        Double numberPositives = positiveNegativeArray[0];
        Double numberNegatives = positiveNegativeArray[1];

        Double part2 = -(numberPositives/ (numberPositives + numberNegatives));

        Double part3a = numberPositives/(numberPositives + numberNegatives);
        Double part3 = ( Math.log( part3a ) / Math.log(2) );

        Double part4 = -(numberNegatives/ (numberPositives + numberNegatives));

        Double part5a = numberNegatives/(numberPositives + numberNegatives);
        Double part5 = ( Math.log( part5a ) / Math.log(2) );

        if(positiveNegativeArray[0].equals(0.0)){
            //then the left hand side is going to be "NaN", so ignore it  [ie: (log 0) = infinite = NaN]
            //return (part4 * part5);
            return 0.0;
        }
        else if(positiveNegativeArray[1].equals(0.0)){
            //then the right hand side is going to be "NaN" so ignore it  [ie: (log 0) = infinite = NaN]
           // return (part2 * part3);
            return 0.0;
        }
        else{
            return (part2 * part3) + (part4 * part5);
        }



    }


    private static void writeToStatisticalInformationArrayList(Attribute parentAttribute, List<Attribute> attributeList, String attributeValueName){

        StringBuilder currentStatisticalInformation = new StringBuilder();

        String parentAttributeName = "No Root Node Chosen Yet";
        Integer nodesOrder = 0;
        if(parentAttribute != null){
            parentAttributeName = parentAttribute.attributeName;
            nodesOrder = parentAttribute.nodesOrder;
        }

        String headerPart1 = "******************************************************************************************************************\n\n";

        String headerPart2 = "                                                   P A R E N T    N O D E                                             \n";

        String headerPart3 = "                                                  -------------------------                                           \n";

        String headerPart4 = " Attribute Name:" + parentAttributeName + "Attribute Order: " + nodesOrder + " , Attribute Value: " + attributeValueName +    "\n";

        String headerPart5 = "*******************************************************************************************************************\n\n";

        currentStatisticalInformation.append(headerPart1);
        currentStatisticalInformation.append(headerPart2);
        currentStatisticalInformation.append(headerPart3);
        currentStatisticalInformation.append(headerPart4);
        currentStatisticalInformation.append(headerPart5);

        for(Attribute currentAttribute : attributeList){

            String attributeNameLabel = "Attribute Name: " + currentAttribute.attributeName +  " ,  Information Gain: " + currentAttribute.informationGain +   "\n";
            currentStatisticalInformation.append(attributeNameLabel);


        }
        currentStatisticalInformation.append("\n");
        statisticalInformation.add(currentStatisticalInformation);

    }


















    private static Subtable getSubtableFromAttributeWithMostInformationContent(Attribute attribute){

        //Map<String, Subtable> values = attribute.values;

        Subtable mostInformationGainSubtable = null;

        for(Subtable currentSubtable : attribute.values){

            if(mostInformationGainSubtable == null && currentSubtable.informationContent != null){

                mostInformationGainSubtable = currentSubtable;

            }
            if(currentSubtable.informationContent == null || mostInformationGainSubtable.informationContent == null){
                //then one or both of these subtables are a pure subtable, so we should continue
                continue;
            }
            if(Double.compare(currentSubtable.informationContent, mostInformationGainSubtable.informationContent) > 0){
                //then our currentSubtable has a greater information content than the mostInformationGainSubtables's information content
                mostInformationGainSubtable = currentSubtable;
            }
        }
        return mostInformationGainSubtable;


    }
}

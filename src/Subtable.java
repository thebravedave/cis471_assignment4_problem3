import org.w3c.dom.Attr;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Subtable  implements Cloneable, Serializable {


    public Subtable clone() throws CloneNotSupportedException{
        return (Subtable) super.clone();
    }

    public Subtable deepClone() throws Exception{

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(this);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        return (Subtable) ois.readObject();

    }

    public String subtableName;


    public List<Integer> daysValueWasFoundOnForAttribute = new ArrayList<Integer>();

    public Attribute childAttribute;

    public Double informationContent;


}

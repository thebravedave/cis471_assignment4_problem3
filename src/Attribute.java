import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Attribute implements Cloneable, Serializable {

    public String attributeName;
    //public Map<String, Double[]> values = new HashMap<String, Double[]>();

    //public Map<String, Subtable> values = new HashMap<String, Subtable>();

    public List<StringBuilder> report = new ArrayList<StringBuilder>();

    public List<Subtable> values = new ArrayList<Subtable>();

    public Double informationGain = null;

    public Integer nodesOrder = 0;



    public Attribute clone() throws CloneNotSupportedException{
        return (Attribute) super.clone();
    }

    public Attribute deepClone() throws Exception{

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(this);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        return (Attribute) ois.readObject();

    }



    /*  p = positive examples   n = negative examples

    I(  (p/p+n), (n/p+n)    )      =            -(p/p+n) *  log2(  p/p+n )  -  (n/p+n) * log2( n/p+n )
     */

}
